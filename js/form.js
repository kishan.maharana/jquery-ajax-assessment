$(document).ready(function(){

// hiding error notes
$('#nameerr').hide();
$('#emailerr').hide();
$('#phoneerr1').hide();
$('#phoneerr2').hide();
$('#phoneerr3').hide();
$('#addresserr').hide();
$('#dateerr').hide();
$('#radioerr').hide();
$('#commerceerr').hide();
$('#checkval').hide();
$('#imgerr').hide();

var global;


//hiding error part complete

//slide event on radio button

$("#science").click(function(){
	$("#sciencelist").slideDown("slow");
	$("#commercelist").slideUp();
});

$("#commerce").click(function(){
	$("#commercelist").slideDown("slow");
	$("#sciencelist").slideUp();
});

// ending slide event on radio button

// On clicking on submit button

$('form').submit(function(event){
	//Name validation
	var namelength = $('#name').val().length;
	if(namelength == 0)
	{
		$('#nameerr').show();
		$('#nameerr').css('color','red');
		return false;
	}
	else
	{
		$('#nameerr').hide();
	}

	//email validation
	var email = $('#email').val();
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!regex.test(email)) 
	{
		$('#emailerr').show();
		$('#emailerr').css('color','red');
		return false;
	}
	else
	{
		$('#emailerr').hide();
	}

	  //phone number validation

	  var phonelength = $('#phone').val().length;
	  if(phonelength < 10)
	  {
	  	$('#phoneerr2').show();
	  	$('#phoneerr2').css('color','red');
	  	return false;
	  }
	  else if(phonelength > 10)
	  {
	  	$('#phoneerr3').show();
	  	$('#phoneerr3').css('color','red');
	  	return false;
	  }
	  else
	  {
	  	$('#phoneerr').hide();
	  }
	//Address validation

	var addresslength = $('#address').val().length;
	if(namelength == 0)
	{
		$('#addresserr').show();
		$('#addresserr').css('color','red');
		return false;
	}
	else
	{
		$('#addresserr').hide();
	}

	//Radio button validation

	if($('#science').is(':checked')) { 
		$('#radioerr').hide(); 
	}
	else if($('#commerce').is(':checked'))
	{
		$('#radioerr').hide(); 
	}
	else
	{
		$('#radioerr').show();
		$('#radioerr').css('color','red');
	}

	//checkbox validation


	var select = $('input[type="checkbox"]:checked');
	if(select.length>=2){
		$('#checkval').hide();
	}
	else{
		$('#checkval').show();
		$('#checkval').css('color','red');
	}



	//end of form validation

	//Applying AJAX for sending form data 

	var that = $(this),
	url=that.attr('action'),
	type=that.attr('method'),
	data={};

	that.find('[name]').each(function(index,value){
		var that = $(this),
		name=that.attr('name'),
		value=that.val();
		data[name] = value;
	});

	$.ajax({
		url: url,
		type: type,
		data: data,
		success:function(response)
		{
			console.log(response);
			if(response)
			{
				console.log('true');
				document.getElementById('alldata').innerHTML =  response;
			}
			else
			{
				console.log('else statement');
			}

		}
	});
		
		read(global); // calling function to show image after form upload

		return false;
	});

	//closing submit button code
	
	//Thumbnail image showing code
	function readURL(input) {
		global=input; // assigning parameter to the global variable
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#pimg').attr('src', e.target.result);
			}

	    reader.readAsDataURL(input.files[0]); // convert to base64 string
	}
}

// function for displaying image whenever a form is submitted

	function read(global) {
		if (global.files && global.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#blah1').attr('src', e.target.result);
			}

	    reader.readAsDataURL(global.files[0]); // convert to base64 string
	}
}

// change event for whenever a image is selected

$("#imgInp").change(function() {
	console.log(this);
	readURL(this);
});
});
