<!DOCTYPE html>
<html>
<head>
	<title>Assessment</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>


	<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> -->
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div class="row my-4">
		<div class="col-12 text-center">
			<h2>Registration Form</h2>
		</div>
	</div>
	<div class="row my-5">
		<div class="col-2">

		</div>
		<div class="col-8">
			<form action="script.php" method="post" id="my_form" class="ajax" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" class="form-control" id="name" name="name" autocomplete="off">
					<p id="nameerr">Field is required</p>
					<label for="email">Email</label>
					<input type="text" name="email" class="form-control" id="email">
					<p id="emailerr">Email is not valid</p>
					<label for="phone">Phone</label>
					<input type="number" class="form-control" name="phone" id="phone">
					<p id="phoneerr1">Field is required</p>
					<p id="phoneerr2">Number should be atleast 10 digits</p>
					<p id="phoneerr3">Number should be atmost 10 digits</p>
					<label for="address">Address</label>
					<input type="text" class="form-control" name="address" id="address">
					<p id="addresserr">Field is required</p>
					<label for="dob">Date of Birth</label>
					<input type="date" class="form-control" name="dob" id="dob">
					<p id="dateerr">Field is required</p>
					<input type="radio" name="stream" id="science">Science<br>

					<input type="radio" name="stream" id="commerce">Commerce<br>
					<p id="radioerr">Please select a stream</p><br>
					<div id="sciencelist">
						<input type="checkbox" name="sub[]" class="form-check-input" id="physics" value="physics">physics<br>
						<input type="checkbox" name="sub[]" class="form-check-input" id="chemistry" value="chemistry">chemistry<br>
						<input type="checkbox" name="sub[]" class="form-check-input" id="math" value="math">math<br>
					</div><br>
					<div id="commercelist">
						<input type="checkbox" name="sub[]" class="form-check-input" id="physics" value="accounting">accounting<br>
						<input type="checkbox" name="sub[]" class="form-check-input" id="chemistry" value="homescience">homescience<br>
						<input type="checkbox" name="sub[]" class="form-check-input" id="math" value="history">history<br>
					</div>
					<p id="checkval">Atleast 2 fields required</p>

					<br>
					<input type='file' id="imgInp" name="image" class="img-fluid" style="height: 200px;width: 200px;"  /><br>
					<img id="pimg" src="#" alt="your image" /><br>
					<p id="imgerr">Image field is required</p>
					<button class="btn btn-success" type="submit" id="submit">submit</button>
				</div>
				<div id="server-results"><!-- For server results --></div>
			</form>

			<!-- showing image after form submission -->

			<img id="blah1" src="#" alt="your image" class="img-fluid" style="height: 200px;width: 200px;" /><br>
			<div id="alldata">

			</div>

		</div>
		<div class="col-2">

		</div>
	</div>

	<script src="js/form.js"></script>
</body>
</html>